import { Component, OnInit } from '@angular/core';
import { ReservationserviceService} from '../reservationservice.service';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { MatSnackBar } from '../../../node_modules/@angular/material';
import { ParkingComponent } from '../parking/parking.component';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.scss']
})
export class ReservationComponent implements OnInit {
  idutilisateur;
  idparking;
  ddebut;
  dfin;
  numeroplace;

  // tslint:disable-next-line:max-line-length
  constructor(private route: ActivatedRoute, private reservationservice: ReservationserviceService, public snack: MatSnackBar, private router: Router) {
    this.route.params.subscribe(data => {
      console.log(data);
      this.idparking = data.idparking;
      this.numeroplace = data.idplace;
      console.log(this.idparking);
      console.log(this.numeroplace);


    });
   }

  ngOnInit() {
  }
  reserver() {
    this.idutilisateur = this.reservationservice.email;
    const postData = {
      idReservation: this.idutilisateur + '-' + this.numeroplace + '-' + this.idparking,
      DateDebut: this.ddebut,
      DateFin: this.dfin,
      utilisateurId: this.dfin,
      placeId: this.idutilisateur
    };
    this.reservationservice.reserver(environment.api_url + '/api/Reservations', postData).subscribe( data => {
      console.log(data);
      this.snack.open('Reservation réussie');
      this.router.navigate([ParkingComponent]);

    }, (err) => {
      this.snack.open('Verifiez les dates. / la place a deja ete reserve', 'close');
    });

  }

}
