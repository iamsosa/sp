import { Component, OnInit } from '@angular/core';
import { ConnexionserviceService } from '../connexionservice.service';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { ParkingComponent } from '../parking/parking.component';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.scss']
})
export class ConnexionComponent implements OnInit {
  email;
  password;


  constructor(private router: Router, private connexionService: ConnexionserviceService) {
    if (connexionService.isLoggedin()) {
      this.router.navigateByUrl('parking');
      }
    }


  ngOnInit() {
  }

  login() {
    const postData = {
      email: this.email,
      password: this.password
    };
    this.connexionService.login(postData).subscribe( data => {
      console.log(data);
      localStorage.setItem('loginToken', JSON.stringify(data));
      localStorage.setItem('email', this.email);

    });
  }
}
