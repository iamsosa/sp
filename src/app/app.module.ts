import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModule } from './material.module';
import { InscriptionComponent } from './inscription/inscription.component';
import { ConnexionComponent } from './connexion/connexion.component';
import { AccueilComponent } from './accueil/accueil.component';
import { ParkingComponent } from './parking/parking.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ParkingserviceService } from './parkingservice.service';
import { HttpClientModule } from '../../node_modules/@angular/common/http';
import { PlaceComponent } from './place/place.component';
import { PlaceserviceService } from './placeservice.service';
import { ReservationComponent } from './reservation/reservation.component';
import { FormsModule } from '@angular/forms';
import { ConnexionserviceService } from './connexionservice.service';
import { ReservationserviceService } from './reservationservice.service';
import {AgmCoreModule} from '@agm/core';



@NgModule({
  declarations: [
    AppComponent,
    InscriptionComponent,
    ConnexionComponent,
    AccueilComponent,
    ParkingComponent,
    PlaceComponent,
    ReservationComponent
    ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCZ2m9EEWuvKjKqAmEBHptaA_0RimqsQiw'
    })

  ],
  providers: [ParkingserviceService, PlaceserviceService, ConnexionserviceService, ReservationserviceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
