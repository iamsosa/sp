import { Injectable } from '@angular/core';
import { HttpClient } from '../../node_modules/@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ParkingserviceService {

  constructor(private http: HttpClient) { }
  getData(url) {
    return this.http.get(url);
  }
}
