import { Component, OnInit } from '@angular/core';
import { ConnexionserviceService } from './connexionservice.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  {
  isLoggedIn;
  latradisson: 14.695815;
  lngradisson: -17.472974;
  latrenaissance: 14.72246;
  lngrenaissance: -17.494923;
  latterrou: 14.676702;
  lngterrou: -17.466167;

  constructor(private router: Router, private connexionService: ConnexionserviceService) {  }
  title = 'app';
  routeChanged() {
      this.isLoggedIn = this.connexionService.isLoggedin();
  }
  loggout() {
    this.connexionService.loggout().subscribe(data => {
      localStorage.removeItem('loginToken');
      this.router.navigateByUrl('login');
    });
  }
}
