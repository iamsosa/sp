import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConnexionserviceService {
  email;
  constructor(private http: HttpClient) { }
  isLoggedin() {
    return localStorage.getItem('loginToken') ? true : false;
  }
  login(data) {
    console.log(data);
    const headers = new HttpHeaders({ 'Content-type': 'application/json' });
    this.email = data.email;
    console.log(this.email);


    return this.http.post(`${environment.api_url}/api/Utilisateurs/login`, data , {headers: headers});
  }
  loggout() {
    let accesToken;
    if ( localStorage.getItem('loginToken')) {
    accesToken = JSON.parse(localStorage.getItem('loginToken')).id;
    }
    return this.http.post(`${environment.api_url}/api/Utilisateurs//logout?access_token=$(accesToken)`, {});
  }
}

