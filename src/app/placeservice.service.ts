import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '../../node_modules/@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PlaceserviceService {

  constructor(private http: HttpClient) { }
  getData(url) {
    return this.http.get(url);

  }

  postData(url, data) {
    console.log(url, data);
    const headers = new HttpHeaders({'Content-type': 'application/json'});
    return this.http.post(url, data , {headers: headers});
  }
  }

