import { Component, OnInit } from '@angular/core';
import { ParkingserviceService } from '../parkingservice.service';
import { environment } from '../../environments/environment';
import { ConnexionserviceService } from '../connexionservice.service';

@Component({
  selector: 'app-parking',
  templateUrl: './parking.component.html',
  styleUrls: ['./parking.component.scss']
})
export class ParkingComponent implements OnInit {
  parking;
  isLoggedIn;
  constructor(private connexionService: ConnexionserviceService, private parkingService: ParkingserviceService) {
    if (connexionService.isLoggedin()) {
      this.isLoggedIn = true;
    }
   }


  ngOnInit() {
    this.parkingService.getData(environment.api_url + '/api/Parkings').subscribe(data => {
      console.log(data);
      this.parking = data;
    });

  }

}
