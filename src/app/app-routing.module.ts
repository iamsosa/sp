import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { ConnexionComponent } from './connexion/connexion.component';
import { ParkingComponent } from './parking/parking.component';
import { PlaceComponent } from './place/place.component';
import { ReservationComponent } from './reservation/reservation.component';

const routes: Routes = [
  { path: 'app', component: AppComponent },
  { path: 'connexion', component: ConnexionComponent },
  { path: 'inscription', component: InscriptionComponent },
  { path: 'parking/:id/place', component: PlaceComponent },
  { path: 'parking', component: ParkingComponent },
  { path: 'place', component: PlaceComponent },
  { path: 'place/:idparking/:idplace/reserver', component: ReservationComponent },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
