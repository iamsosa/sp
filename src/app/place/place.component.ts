import { Component, OnInit, Input } from '@angular/core';
import { environment } from '../../environments/environment';
import { PlaceserviceService } from '../placeservice.service';
import { ActivatedRoute } from '../../../node_modules/@angular/router';


@Component({
  selector: 'app-place',
  templateUrl: './place.component.html',
  styleUrls: ['./place.component.scss']
})
export class PlaceComponent implements OnInit {
  place;
  pageLength: number;
  pageSize = 5;
  idparking;
  constructor(private route: ActivatedRoute, private placeService: PlaceserviceService ) {
    this.route.params.subscribe(data => {
      console.log(data);
      this.idparking = data.id;

    });
   }


  ngOnInit() {
    // tslint:disable-next-line:max-line-length
    this.placeService.getData(environment.api_url + '/api/Places?filter[limit]=' + this.pageSize + '&filter[where][Reservable]=' + true).subscribe(data => {
      console.log(data);
      this.place = data;
    });
    this.placeService.getData(environment.api_url + '/api/Places/count' + '?filter[where][Reservable]=' + true).subscribe(data => {
      console.log(data);
      this.pageLength = data['count'];
    });
    this.placeService.getData(environment.api_url + '/api/Parkings/' + this.idparking).subscribe(data => {
      console.log(data);
    });
  }
  pageEvent(ev) {
    console.log(ev);
    const pageIndex = ev.pageIndex;
    const pageSize = ev.pageSize;
    const skip = pageIndex + pageSize;
    // tslint:disable-next-line:max-line-length
    this.placeService.getData(environment.api_url + '/api/Places/?filter[limit]=' + this.pageSize + '&filter[skip]=' + skip + '&filter[where][Reservable]=' + true).subscribe(data => {
      console.log(data);
      this.place = data;
    });
  }
  }

